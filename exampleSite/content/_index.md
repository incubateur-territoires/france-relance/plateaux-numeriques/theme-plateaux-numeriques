---
title: Accueil
description: Site officiel de votre commune
subtitle: ''
headless: true
header: "/media/village.jpg"
sections:
- actualites
widgets:
- acces-rapides

---
# Bienvenue !

Texte de présentation court de votre commune. Sa situation géographique, son nombre d’habitants, ses particularités. Nullam quis risus eget urna mollis ornare vel eu leo. Curabitur blandit tempus porttitor. Nulla vitae elit libero, a pharetra augue. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.