---
title: Déclaration de conformité au Référentiel général d’accessibilité pour les administrations (RGAA)
description: ""
subtitle: ""
icon: ""

---

## Introduction

Cette page n’est pas une page d’aide, mais une déclaration de conformité au [RGAA 4.1](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/) qui vise à définir le niveau d’accessibilité général constaté sur le site conformément à la réglementation. Cette page est obligatoire pour être conforme au RGAA 4.1. Pour des aides relatives à la navigation et aux aménagements particuliers du site, visitez la page [Accessibilité](/accessibilite).

La mairie de Lalouvesc s’engage à rendre son site web accessible conformément à l’article 47 de la loi n°2005-102 du 11 février 2005.

À cette fin, elle met en œuvre la stratégie et les actions suivantes :
- Schéma pluriannuel de mise en accessibilité 2021-2023 (à venir),
- Actions réalisées en 2021 (en cours)

Cette déclaration d’accessibilité s’applique à www.lalouvesc.fr.

## État de conformité

Le site web de la mairie de Lalouvesc www.lalouvesc.fr est en **conformité partielle** avec le référentiel général d’amélioration de l’accessibilité (RGAA), version 4.1 en raison des non-conformités et des dérogations énumérées ci-dessous.

## Résultats des tests

L’audit de conformité réalisé par [Access42](https://access42.net/) révèle que :
- **50%** des critères du RGAA version 4.1 sont respectés, avec 45,24% de conformité au niveau simple A (A) et 62,50% de conformité au niveau double A (AA)
- Le taux moyen de conformité du site s’élève à **85,62%**

## Contenus non accessibles

### Non-conformité

Le widget de réservation de la page Camping concentre l’essentiel des erreurs identifiées sur le site. Les non-conformités les plus bloquantes pour les utilisateurs concernent :
- les scripts et l’utilisation de l’API ARIA ;
- les formulaires (étiquettes et gestion des erreurs) ;
- les contrastes de couleur ;
- les alternatives des images, en particulier pour les images complexes ;
- la structuration des contenus ;
- les tableaux.

Nous allons contacter [Alliance Réseaux](https://www.alliance-reseaux.com/), le fournisseur de ce widget, pour leur faire part des problèmes identifiés afin qu’ils les corrigent au plus vite.

Les autres non-conformités identifiées seront corrigées par les équipes de [Plateaux numériques](https://plateaux-numeriques.fr/) d’ici la fin de l’automne 2021.

## Établissement de cette déclaration d’accessibilité

Cette déclaration a été établie le 23/09/2021. 

### Technologies utilisées pour la réalisation du site

- HTML5
- CSS
- JavaScript
- Hugo
- Forestry

### Environnement de test

Les vérifications de restitution de contenus ont été réalisées sur la base de la combinaison fournie par la base de référence du RGAA 4.1, avec les versions suivantes :

| Technologie d’assistance | Navigateur |
| ------------------------ | ---------- |
| NVDA 2021.1 | Firefox 89 |
| JAWS 2019 | Firefox 89 |
| VoiceOver (macOS 11.4) | Safari 14.1 |


### Pages du site ayant fait l’objet de la vérification de conformité

1. Accueil : https://www.lalouvesc.fr/
2. Contact: https://www.lalouvesc.fr/contact/
3. Mention légales : https://www.lalouvesc.fr/mentions-legales/
4. Aide et accessibilité : https://www.lalouvesc.fr/aide-accessibilite/
5. Plan du site : https://www.lalouvesc.fr/plan-du-site/
6. Camping : https://www.lalouvesc.fr/decouvrir-bouger/hebergement-restauration/camping/

## Retour d’information et contact

Si vous n’arrivez pas à accéder à un contenu ou à un service, vous pouvez nous contacter pour être orienté vers une alternative accessible ou obtenir le contenu sous une autre forme.

[Accéder à la page contact](/contact)

### Voies de recours

Si vous constatez un défaut d’accessibilité qui vous empêche d’accéder à un contenu ou une fonctionnalité du site, que vous nous en informez et que vous ne parvenez pas à obtenir une réponse rapide de notre part, vous êtes en droit de faire parvenir vos doléances ou demande de saisine au Défenseur des Droits. Plusieurs moyens sont à votre disposition :

- un [formulaire de contact](https://formulaire.defenseurdesdroits.fr/)
- un numéro de téléphone : <a href="tel:0969390000">09 69 39 00 00</a>
- la [liste du ou des délégués de votre région](https://information.defenseurdesdroits.fr/CC1710/?l=DEL) avec leurs informations de contact directs
- une adresse postale (courrier gratuit, sans affranchissement) : Le Défenseur des droits – Libre réponse 71120 – 75342 Paris CEDEX 07
