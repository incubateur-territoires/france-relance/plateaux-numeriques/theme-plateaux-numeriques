---
title: Papiers et citoyenneté
description: "État civil, authentification, élections, recensement"
subtitle: 
icon: "📑"
weight: 1
menu:
  main:
    identifier: papiers-citoyennete
    weight: 1
    parent: demarches

---

## Extraits / déclaration, décès, mariage, naissance

Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Sed posuere consectetur est at lobortis. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum.

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Donec id elit non mi porta gravida at eget metus. 
- Cras justo odio, dapibus ac facilisis in, egestas eget quam. 
- Maecenas sed diam eget risus varius blandit sit amet non magna. 
- Aenean lacinia bibendum nulla sed consectetur.

## Recensement

Etiam porta sem malesuada magna mollis euismod. Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis ornare vel eu leo. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas sed diam eget risus varius blandit sit amet non magna. Sed posuere consectetur est at lobortis.

## Inscription sur les listes électorales

Nullam quis risus eget urna mollis ornare vel eu leo. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Lorem ipsum dolor sit amet, consectetur adipiscing elit.

Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit sit amet non magna. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.

## Vie conjugale (mariage, PACS, union libre)

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Vestibulum id ligula porta felis euismod semper. Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum. Maecenas faucibus mollis interdum.

## Cimetière

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Nullam id dolor id nibh ultricies vehicula ut id elit. Donec id elit non mi porta gravida at eget metus.

## Autres démarches (carte d’identité, casier judicière, carte grise, portail service-public)

Consulter le site officiel de l’administration française, rubrique "Papiers – Citoyenneté".
[www.service-public.fr/particuliers/vosdroits/N19810](https://www.service-public.fr/particuliers/vosdroits/N19810)