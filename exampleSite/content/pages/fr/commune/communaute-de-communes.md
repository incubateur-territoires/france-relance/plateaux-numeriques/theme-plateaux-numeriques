---
title: Communauté de communes
description: ''
subtitle: ''
icon: "🌐"
weight: 4
date: 
header: ''
menu:
  main:
    weight: 4
    identifier: communaute-de-communes
    parent: commune

---

## Présentation

La Communauté de communes comprend huit Communes : Votre commune, Commune A, Commune B, Commune C, Commune D, Commune E, Commune F et Commune G. Le Conseil communautaire est composé de 24 élus issus des conseils municipaux des communes membres. Votre commune y est représentée par son Maire, Prénom Nom, et son premier adjoint, Prénom Nom.

[Compte-rendus du Conseil sur le site de la Communauté de communes](#)

## Responsabilités

### Compétences obligatoires

* Développement économique, recherche, commerce et artisanat
* Gestion de l’espace (agriculture, forêts et paysages)
* Promotion du tourisme dont la création d’office de tourisme et de zones d’activités touristiques
* Gestion des ordures ménagères et des déchets
* Aménagement, entretien et gestion des aires d’accueil des gens du voyage

### Les compétences optionnelles adoptées

* Enfance et jeunesse
* Énergies renouvelables
* Réseaux de télécommunications numériques
* Gestion de la voirie communautaire
* Gestion des chiens errants
* Janvier 2018 : GEMAPI (Gestion des Milieux Aquatiques & Prévention des Inondations)

## Représentants de Votre commmune au sein de la Communauté de communes

* Conseil communautaire : Prénom Nom, Prénom Nom
* Commission gestion de l’espace : Prénom Nom 
* Commission tourisme : Prénom Nom (président), Prénom Nom
* Commission enfance et jeunesse : Prénom Nom
* Commission finance : Prénom Nom
* Commission développement économique et durable : Prénom Nom
* Commission déchet : Prénom Nom
* Commission voirie : Prénom Nom
* Commission ADN communication : Prénom Nom
* Commission travaux et bâtiments : Prénom Nom

## Pour en savoir plus

[Site de la Communauté de commune](#)