---
title: Test page blocks
breadcrumb_title: "Blocks"
url: "/fr/blocks/"
header: media/mairie.jpg
position: 7
icon: "📜"
weight: 2
menu:
  main:
    identifier: blocks
    weight: 2
    parent: commune
bodyclass: contain
image:
  id: "36bca91e-94c6-49a2-bfaa-52e4b5d531be"
  alt: ""
  credit: >
    <p>Crédit photo.<br></p>

description: >
  Meta Description Etiam porta sem malesuada magna mollis euismod.
description_short: >
  Texte du chapô. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum.
header_text: >-
  Titre affiché dans le header.
legacy_text: >

blocks:
  - template: chapter
    title: >-
      Test chapitre 1
    position: 1
    data:
      text: >-
        <p>Texte. Lorem ipsum dolor sit amet, 2<sup>e</sup> adipiscing elit. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Sed <i>posuere consectetur</i> est at lobortis.</p><ul> <li>Unordered list item 1 : Donec id elit non mi porta gravida at eget metus.</li> <li>Unordered list item 2 : Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</li> <li>Unordered list item 3 : Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</li> </ul><p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. <b>Curabitur blandit tempus porttitor</b>. </p><ol> <li>Ordered list item 1 : Donec id elit non mi porta gravida at eget metus.</li> <li>Ordered list item 2 : Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</li> <li>Ordered list item 3 : Fusce dapibus, tellus ac cursus commodo, tortor mauris  condimentum nibh, ut fermentum massa justo sit amet risus.</li> </ol><p></p><p>Pour en savoir plus : <a href="https://plateaux-numeriques.fr/" target="_blank">sit amet fermentum</a>. </p><p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.  Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla.  Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam quis risus eget urna mollis ornare vel eu leo. Cras justo odio, dapibus ac facilisis in, egestas eget quam. </p><p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula porta felis euismod semper.</p>
      notes: >-
        <p>Notes. Curabitur blandit tempus porttitor. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam id dolor id nibh ultricies vehicula ut id elit. Sed posuere consectetur est at lobortis.</p>
  - template: chapter
    title: >-
      Test chapitre 2
    position: 2
    data:
      text: >-
        <h3>Sous-titre de niveau 3</h3><p>Texte. Lorem ipsum dolor sit amet, 2<sup>e</sup> adipiscing elit. Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Sed <i>posuere consectetur</i> est at lobortis.</p><ul> <li>Unordered list item 1 : Donec id elit non mi porta gravida at eget metus.</li> <li>Unordered list item 2 : Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</li> <li>Unordered list item 3 : Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</li> </ul><p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras mattis consectetur purus sit amet fermentum. <b>Curabitur blandit tempus porttitor</b>. </p><ol> <li>Ordered list item 1 : Donec id elit non mi porta gravida at eget metus.</li> <li>Ordered list item 2 : Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.</li> <li>Ordered list item 3 : Fusce dapibus, tellus ac cursus commodo, tortor mauris  condimentum nibh, ut fermentum massa justo sit amet risus.</li> </ol><p></p><p>Pour en savoir plus : <a href="https://plateaux-numeriques.fr/" target="_blank">sit amet fermentum</a>. </p><h3>Sous-titre de niveau 3</h3><p>Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.  Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla.  Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam quis risus eget urna mollis ornare vel eu leo. Cras justo odio, dapibus ac facilisis in, egestas eget quam. </p><p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum id ligula porta felis euismod semper.</p>
  - template: image
    title: >-
      Titre de l’image
    position: 3
    data:
      text: >-
        <p>Texte de l'image. Nullam quis risus eget urna mollis ornare vel eu leo. Nulla vitae elit libero, a pharetra augue.<br></p>
      image:
        id: "49aa27d6-2acc-4b70-9fcc-0a63522874a6"
        file: "49aa27d6-2acc-4b70-9fcc-0a63522874a6"
        alt: >-
          Description textuelle de l'image
        credit: >-
          Crédit de l'image
  - template: video
    title: >-
      Titre de la vidéo
    position: 4
    data:
      url: >-
        https://youtu.be/qtIqKaDlqXo
      transcription: >-
        Transcription textuelle du contenu.

        Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum.
        Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam id dolor id nibh ultricies vehicula ut id elit. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.

        Sed posuere consectetur est at lobortis. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  - template: datatable
    title: >-
      Test tableau
    position: 5
    data:
  - template: key_figures
    title: ""
    position: 6
    data:
      figures:
        - number: >-
            43
          unit: >-
            °C
          description: >-
            Record de chaleur 202 en France à Archachon
        - number: >-
            53.77
          unit: >-
            %
          description: >-
            Taux d'abstention au 2nd tour des élections législatives 2022 en France.
  - template: gallery
    title: >-
      Test gallerie grille
    position: 7
    data:
      layout: grid
      images:
        - id: "fb8ecd8d-3819-4013-8ea3-cf715728af28"
          file: "fb8ecd8d-3819-4013-8ea3-cf715728af28"
          alt: >-

          credit: >-
            Crédit de l'image 1
          text: >-
            Texte de l'image 1.

        - id: "67cce7f6-c311-4ba2-b5a8-7b11fc83912d"
          file: "67cce7f6-c311-4ba2-b5a8-7b11fc83912d"
          alt: >-

          credit: >-
            Crédit de l'image 2
          text: >-
            Texte de l'image 2.

        - id: "e5ad50ec-509a-4ab4-9420-30fc77251271"
          file: "e5ad50ec-509a-4ab4-9420-30fc77251271"
          alt: >-

          credit: >-
            Crédit de l'image 3
          text: >-
            Texte de l'image 3.

        - id: "fb8ecd8d-3819-4013-8ea3-cf715728af28"
          file: "fb8ecd8d-3819-4013-8ea3-cf715728af28"
          alt: >-

          credit: >-

          text: >-
            Texte de l'image 4.

  - template: gallery
    title: >-
      Test gallerie carousel
    position: 8
    data:
      layout: carousel
      images:
        - id: "fb8ecd8d-3819-4013-8ea3-cf715728af28"
          file: "fb8ecd8d-3819-4013-8ea3-cf715728af28"
          alt: >-

          credit: >-
            Crédit de l'image 1
          text: >-
            Texte de l'image 1.

        - id: "67cce7f6-c311-4ba2-b5a8-7b11fc83912d"
          file: "67cce7f6-c311-4ba2-b5a8-7b11fc83912d"
          alt: >-

          credit: >-
            Crédit de l'image 2
          text: >-
            Texte de l'image 2.

        - id: "e5ad50ec-509a-4ab4-9420-30fc77251271"
          file: "e5ad50ec-509a-4ab4-9420-30fc77251271"
          alt: >-

          credit: >-
            Crédit de l'image 3
          text: >-
            Texte de l'image 3.          
  - template: call_to_action
    title: >-
      Test CTA
    position: 9
    data:
      text: >-
        <p>Texte. Maecenas sed diam eget risus varius blandit sit amet non magna. Vestibulum id ligula porta felis euismod semper. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam quis risus eget urna mollis ornare vel eu leo.</p>
      image:
        id: "7414e3a4-3d2c-4181-9f61-da2edb00d2e0"
        file: "7414e3a4-3d2c-4181-9f61-da2edb00d2e0"
        alt: >-
          Carte de France
        credit: >-
          Ministère de l'intérieur
      button:
        text: >-
          Bouton principal
        url: >-
          https://www.resultats-elections.interieur.gouv.fr/legislatives-2022/index.html
        target_blank: true
      button_secondary:
        text: >-
          Bouton secondaire
        url: >-
          https://www.resultats-elections.interieur.gouv.fr/legislatives-2022/FE.html
        target_blank: false
      button_tertiary:
        text: >-
          Bouton tertiaire
        url: >-
          https://www.resultats-elections.interieur.gouv.fr/legislatives-2022/ELUS.html
        target_blank: false
  - template: testimonials
    title: >-
      Test testimonials
    position: 10
    data:
      testimonials:
        - text: >-
            Ceux qui utilisent négligemment les miracles de la science en ne les comprenant pas plus qu’une vache ne comprend la botanique des plantes qu’elle broute avec plaisir devraient tous avoir honte
          author: >-
            Albert Einstein
          job: >-
            Physicien
        - text: >-
            L'éducation, c'est la famille qui la donne ; l'instruction, c'est l'Etat qui la doit.
          author: >-
            Victor Hugo
          job: >-
            Érivain
          photo: "c8e016a6-dd3f-4417-8660-ed72bd42cba0"
        - text: >-
            L'éducation, c'est la famille qui la donne ; l'instruction, c'est l'Etat qui la doit.
          author: >-
            Victor Hugo
          job: >-

          photo: "c8e016a6-dd3f-4417-8660-ed72bd42cba0"
  - template: timeline
    title: >-
      Test timeline
    position: 11
    data:
      events:
        - title: >-
            Titre de l'événement 1
          text: >-
            Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
        - title: >-
            Titre de l'événement 2
          text: >-
            Curabitur blandit tempus porttitor. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur.
        - title: >-
            Titre de l'événement 3
          text: >-
            Cras justo odio, dapibus ac facilisis in, egestas eget quam. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.
  - template: pages
    title: >-
      Test pages liste avec descriptions courtes
    position: 12
    data:
      show_main_description: false
      show_descriptions: true
      show_images: false
      layout: list
      pages:
        - page: /fr/accessibilite/
          slug: /fr/accessibilite/
        - page: /fr/politique-de-confidentialite/
          slug: /fr/politique-de-confidentialite/
  - template: partners
    title: >-
      Test partenaires
    position: 13
    data:
      - name: >
          Covaldor
        url: >-
          https://www.cauvaldor.fr/
        logo: "33a1dbaf-1ad5-4079-91ba-32edc893d0e9"
      - name: >
          Département du Lot
        url: >-
          https://lot.fr/
        logo: "e5ad50ec-509a-4ab4-9420-30fc77251271"
  - template: files
    title: >-
      Test fichier
    position: 14
    data:
      files:
        - title: >-
            CR réunion 2022-03-01
          id: "234017af-64cf-4ff2-ab9a-56391148c28e"
  - template: definitions
    title: >-
      Test définitions
    position: 15
    data:
      - title: >-
          Communauté de communes
        text: >-
          Les communautés de communes ont été créées en France par la loi n° 99-586 du 12 juillet 1999 et constituent un ensemble de communes formant un seul tenant et sans enclave. Mais cette dernière caractéristique tolère des exceptions notamment pour les communautés qui existaient avant la publication de la loi du 12 juillet 1999. Elles ont la forme juridique d’un EPCI (Établissement public de coopération intercommunale). Les communautés de communes ont vu le jour dans le but de développer l’aménagement de l’espace, les collaborations techniques et administratives, les synergies favorisant les économies, la solidarité entre les communes participantes.
      - title: >-
          Marché public
        text: >-
          Les marchés publics sont des contrats conclus à titre onéreux avec des personnes publiques ou privées par des personnes morales de droit public (collectivités territoriales, Etat, établissements publics) pour l'acquisition de prestations de services, de fournitures ou de travaux. Leur régime fait l'objet d'une codification (Décret 15-2004 du 7 janvier 2004 portant Code des marchés publics). Les principes généraux de la commande publique énoncés par le code : Liberté d'accès des entreprises à la commande publique Egalité de traitement des entreprises par les acheteurs Transparence de la mise en œuvre des procédures d'achat Efficacité de la commande publique et de la bonne utilisation de l'argent public Des règles de forme (modalités de publicité et de mise en concurrence, procédures de passation des marchés) et des règles de fond (dispositions réglementaires devant figurer dans les documents contractuels) La quasi totalité des achats effectués par les acheteurs publics relèvent des règles définies par le code, qu'il s'agisse : De l'achat de travaux de bâtiment ou de génie civil neufs ou d'entretien De l'acquisition de produits ou matériels de quelque nature que ce soit (mobilier, informatique, véhicules, produits alimentaires, logiciels, carburants, produits d'entretien, équipements techniques et logistiques, médicaments...) De la passation de contrats de location, de location-vente, de crédit-bail ayant trait aux matériels et aux produits (ex : location de matériel informatique ou de véhicules) D'approvisionnement en prestations de services (maintenance d'équipement, nettoyage de locaux, gardiennage de sites, reprographie, transport, études, développement de logiciels...)
      - title: >-
          PLU
        text: >-
          Un plan local d'urbanisme (PLU) est un document fixant les normes de planification de l'urbanisme pour une commune ou un groupement de communes. Le PLU établit ainsi les principales règles applicables à l'utilisation du sol sur un territoire déterminé, notamment en matière de permis de construire. Il est élaboré par la commune ou l'EPCI (en cas de groupement de communes). Après son élaboration, le PLU peut éventuellement être révisé ou modifié. Suite à une loi de décembre 2000, le PLU a succédé à l'ancien plan d'occupation des sols (POS).
  - template: embed
    title: >-
      Titre de l’iframe
    position: 16
    data:
      code: >-
        <iframe width="100%" height="440" frameborder="0" src="https://www.openstreetmap.org/export/embed.html?bbox=1.4917373657226562%2C44.75904567594879%2C1.7183303833007815%2C44.8805661497192&amp;layer=mapnik"></iframe>
      transcription: >-

---
