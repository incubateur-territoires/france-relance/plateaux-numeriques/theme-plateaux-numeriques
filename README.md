# Plateaux numériques le thème

> Ce thème est un thème de démarrage pour aider à développer des thèmes `goHugo` à l’aide du framework CSS `TailwindCSS`. Ce n’est *** pas *** un thème autonome prêt à l’emploi.

## Développements

### Versions

Le développement du thème respecte la convention SemVer (Semantic Versionning). L’objectif est de passer par un modèle de releases à court terme. Lire : <https://docs.gitlab.com/ee/user/project/releases/>

### Évolutions

  * [x] Compléter le README
  * [x] Utiliser le Conventional Commits (<https://www.conventionalcommits.org/en/v1.0.0/>)
  * [x] Choisir entre `GitFlow` et `le développement basé sur le tronc`
  * [x] Faire du thème un module (attente d’une url définitive)
  * [x] Supprimer la dépendance à `TailwindCSS` et `PostCSS`
  * [x] Simplifier les `partials`.
  * [ ] Lien architecture de contenu <=> édition pour la structure de menus (ex dans `data/menus/lenomdemonmenu.yml`)
  * [ ] Gestion arborescence multi-niveau avec Osuny
  * [ ] Version étendu de goHugo () et test CI
  * [ ] Tester le thème as a module (Code d’accès)
  * [x] Structure prête à utiliser en local :: `content` de netlify CMS (exampleSite)
  * [ ] Dépôt exemple :: Structure de contenu. (définition des cas possibles: Langues, Arborescence)
  * [ ] Utiliser le Versionning Sémantique (SemVer)
  * [ ] Créer un module pour utiliser Netlify CMS ([Self-hosted Netlify CMS](https://medium.com/@cjameswilkinson/self-hosted-netlify-cms-with-middleman-gitlab-and-ubuntu-50b7f1021bcd) - [NetlifyCMS Hugo Module](https://github.com/theNewDynamic/hugo-module-tnd-netlifycms))
  * [ ] Modulariser les composants (Voir comment créer des modules).

### Conventional Commits

Pour utiliser le Conventional Commits, lire cette page : <https://www.conventionalcommits.org/en/v1.0.0/>. Voici un exemple de termes à utiliser pour préfixer les commits.

 * `fix:`
 * `feat:`
 * `build:`
 * `chore:`
 * `ci:`
 * `docs:`
 * `style:`
 * `refactor:`
 * `perf:`
 * `test:`


### Nommage des branches

Pour suivre les travaux, les branches doivent respecter un espace de nommage. Avec Git, il est possible de classer les branches en les préfixant de cette marnière : `espacedenommage/branche`, ex : `contenu/agenda`.

Voici un exemple d’espace de nommage :

 * monitoring
 * contenu
 * edition
 * deploiement
 * optimisation
 * documentation
 * …

Les branches devraient correspondre en grande partie à des modules `goHugo`. Elle doivent permettre de conscrire et répartir des tâches (objectifs à atteindre) dans le temps.

## Installation

### Prérequis

### goHugo

Version minimum de `goHugo` requise : `v0.97.0`.


### Comment utiliser ce thème

- Cloner et renommer le dépôt de code

```bash
git clone https://gitlab.com/plateaux-numeriques/plateaux-numeriques-theme plateaux-numeriques-theme
```

- Éditer le fichier `config.toml` dans `exampleSite/` pour appeler le thème `plateaux-numeriques-theme`

```toml
# in config.toml
theme = "plateaux-numeriques-theme" # le nom du theme
```

- Ligne de commande pour démarrer à partir du répertoire de contenu : `exampleSite`

```bash
hugo server -s exampleSite --themesDir=../.. --disableFastRender
```

### Démarrer à le thème dans un dépôt `goHugo`

- Démarrer un nouveau site `goHugo`

```bash
hugo new site nouveau-site
```

- Accéder au répertoire `themes` et cloner le dépôt de code du thème

```bash
cd nouveau-site/themes
git clone https://gitlab.com/plateaux-numeriques/plateaux-numeriques-theme plateaux-numeriques-theme
```

- Accéder au répertoire `themes/plateaux-numeriques-theme` et éditer le fichier `config.toml` dans `nouveau-site/` pour appeler le thème `plateaux-numeriques-theme`

```toml
# in config.toml
theme = "plateaux-numeriques-theme" # your theme name here
```

- Accéder au la racine de ce nouveau site `goHugo` et lancer un serveur `goHugo` pour accéder au site en local sur votre navigateur.

```bash
cd nouveau-site
hugo server --disableFastRender
```

Votre contenu doit être ajouter le répertoire `nouveau-site/content`, le développement de la mise en page du site se fait dans `nouveau-site/themes/plateaux-numeriques-theme/layout`.

## Usage

### Assistants

Les assistants suivants pour la phase de développement (non visibles en production) sont inclus :

- `/partials/debug/dev-parameters.html`, qui montre les paramètres de base de la page Hugo
- `/partials/debug/dev-size-indicator.html`, qui affiche un cercle flottant dans le coin supérieur droit pour indiquer breakpoints CSS de Tailwind CSS.

Vous pouvez supprimer/désactiver ces assistants, en commentant les lignes correspondantes dans : `/layouts/_default/baseof.html`.

## Déploiement

### Variables d’environnement

Pour faire la distinction entre les environnements de `développement` et de `production`, ajoutez une variable d’environnement `HUGO_ENV = "production"` aux paramètres de votre site sous `Parameters` → `Build & deploy` → `Environnement`.

Où utiliser `netlify.toml` pour une [configuration basée sur des fichiers ](https://docs.netlify.com/configure-builds/file-based-configuration/).
