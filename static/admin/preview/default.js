var DefaultPreview = createClass({
  render: function() {
    var entry = this.props.entry;
    var image = entry.getIn(['data', 'header']);
    var header = this.props.getAsset(image);
    var title = entry.getIn(['data', 'title']);
    var subtitle = entry.getIn(['data', 'subtitle']);
    var icon = entry.getIn(['data', 'icon']);
    if (image) {
      return h('div', {},
        h('header', {id: "page-header", "className": "w-full h-40 md:h-80 bg-gray-100"},
          h('img', {src: header.toString(), "className": "w-full h-full object-cover"})
        ),
        h('section', {"className": "pb-12 px-6 max-w-4xl"},
          h('div', {"className": "pb-4 print:pb-3 border-b"},
            h('span', {"className": "inline-block text-6xl mb-8 -mt-8"}, icon),
            h('h1', {"className": "text-gray-900 mb-2"}, title),
            h('h2', {"className": "text-lg text-mitigate pb-6"}, subtitle)
          ),
          h('div', {"className": "prose lg:prose-lg"}, this.props.widgetFor('body'))
        )
      );
    }
    else {
      return h('div', {"className": "mt-20"},
        h('section', {"className": "pb-12 px-6 max-w-4xl"},
          h('div', {"className": "pb-4 print:pb-3 border-b"},
            h('span', {"className": "inline-block text-6xl mb-8 -mt-8"}, icon),
            h('h1', {"className": "text-gray-900 mb-2"}, title),
            h('h2', {"className": "text-lg text-mitigate pb-6"}, subtitle)
          ),
          h('div', {"className": "prose lg:prose-lg"}, this.props.widgetFor('body'))
        )
      );
    }
  }
});
for (var i = 0; i < defaultCollections.length; i++) {
  CMS.registerPreviewTemplate(defaultCollections[i], DefaultPreview);
}
