var NewsPreview = createClass({
  render: function() {
    var entry = this.props.entry;
    var image = entry.getIn(['data', 'header']);
    var header = this.props.getAsset(image);
    var title = entry.getIn(['data', 'title']);
    var subtitle = entry.getIn(['data', 'subtitle']);
    return h('div', {},
      h('header', {id: "page-header", "className": "w-full h-40 md:h-80 bg-gray-100"},
        h('img', {src: header.toString(), "className": "w-full h-full object-cover"})
      ),
      h('section', {"className": "pb-3 pt-3 px-6 max-w-4xl"},
        h('div', {"className": "pb-4 print:pb-3 border-b"},
          h('h1', {"className": "text-gray-900 mb-2"}, title),
          h('h2', {"className": "text-lg text-mitigate pb-6"}, subtitle)
        ),
        h('div', {"className": "prose lg:prose-lg"}, this.props.widgetFor('body'))
      )
    );
  }
});
CMS.registerPreviewTemplate("actualites", NewsPreview);
