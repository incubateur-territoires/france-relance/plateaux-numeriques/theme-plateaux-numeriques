---
title: >
  Accessibilité
url: "/accessibilite/"
description: >
  Tout savoir sur l’aide à la navigation et l’accessibilité numérique du site de votre mairie.
header_text: >
  Tout savoir sur l’aide à la navigation et l’accessibilité numérique du site de votre mairie.

---

## Liens d’accès rapide

Placés en haut de page, ces liens permettent d’accéder directement à la partie recherchée (sans avoir à parcourir des informations non souhaitées) :

- « Aller au contenu » permet d’aller directement au contenu de la page
- « Aller à la navigation » permet d’aller directement au menu principal
- « Aller à la recherche » active le champ de recherche

## Retour à la page d’accueil du site

Le logo en haut à gauche vous permet, par un simple clic, de retourner sur la page d’accueil.

## Moteur de recherche

Sur l’ensemble des pages, le champ de saisie du moteur de recherche est placé en haut de page.
Lorsque vous effectuez une recherche, le moteur de recherche liste les pages du site qui correspondent le mieux à votre requête.

## Affichage

Vous pouvez augmenter et diminueur la taille d’affichage des textes à l’aide de la fonction zoom de votre navigateur.

## Déclaration de conformité

Le contrôle permettant d’établir une déclaration de conformité, conformément aux termes de la loi, a été réalisé durant l’été 2021. Pour garantir la sincérité et l’indépendance de l’audit, ce contrôle a été effectué par [Access42](https://access42.net/), un intervenant externe spécialisé.

[Consulter la déclaration de conformité](/declaration-conformite-rgaa/)

## Amélioration et contact

Vous pouvez contribuer à améliorer l’accessibilité du site.

N’hésitez pas à nous signaler les problèmes éventuels que vous rencontrez et qui vous empêchent d’accéder à un contenu ou à une fonctionnalité du site.

[Nous contacter](/contact/)